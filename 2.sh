#!/bin/sh
# encoding: utf-8
tar zxf httpd-2.4.27.tar.gz
cd httpd-2.4.27
yum install zlib-devel pcre-devel pcre
./configure \
--prefix=/usr/local/apache \
--with-apr=/usr/local/apr \
--with-apr-util=/usr/local/apr-util/ \
--enable-so \
--enable-deflate=shared \
--enabled-expires=shared \
--enabled-rewrited=shared \
--enabled-static-support

make
make install
cd /usr/local/apache/bin
./httpd -v
echo "hello"
