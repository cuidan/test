#!/bin/sh
# encoding: utf-8
#!/bin/sh
for i in {1..30}
do
ping -c2 10.0.0.$i &> /dev/null

if [ $? -eq 1 ]; then
echo "10.0.0.$i is down"
else
echo "10.0.0.$i is ok"
fi